#!/bin/bash

name=$1
script_name=
output=
directory=

IN="$(find PlayerAlgorithms -name "$name.*")"
arr=(${IN//./ })
type=${arr[1]}


if [[ $type = "cpp" ]]; then
	echo "cpp"
elif [[ $type = "c" ]]; then
	echo "c"
elif [[ $type = "py" ]]; then
	cd PlayerAlgorithmControllers/PythonSrc/
	$(python BoardControllerPythonInterface.py $name)
elif [[ $type = *".m"* ]]; then
	echo ".m"
elif [[ $type = "java" ]]; then
	cd PlayerAlgorithmControllers/JavaSrc/classes
	$(java BoardControllerJavaInterface $name)
else 
	echo "You cannot run this file"
fi
