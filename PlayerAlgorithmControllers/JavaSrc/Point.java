public class Point {
		public int x;
		public int y;
		public String type;

		public Point(int x, int y, String type) {
			this.x = x;
			this.y = y;
			this.type = type;
		}

		@Override
		public String toString() {
			return this.type + " "  + String.valueOf(this.x) + " " + String.valueOf(this.y);
		}
	}