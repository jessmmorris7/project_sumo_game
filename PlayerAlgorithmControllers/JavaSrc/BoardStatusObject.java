import java.io.*;
import java.util.*;

public class BoardStatusObject {

	public ArrayList<Point> playersList;
	public ArrayList<Point> pickupList;
	public ArrayList<Point> wallList;

	public BoardStatusObject() {
		this.playersList = new ArrayList<Point>();
		this.pickupList = new ArrayList<Point>();
		this.wallList = new ArrayList<Point>();
	}

	public ArrayList<Point> getPlayersList() {
		return this.playersList;
	}
	public ArrayList<Point> getPickUpList() {
		return this.pickupList;
	}
	public ArrayList<Point> getWallList() {
		return this.wallList;
	}


}