import java.io.*;
import java.util.*;

public class BoardStatusController {

	private static final String BOARD_STATUS = "/Users/jessemorris/Desktop/Code/project_grand_autism/board_text_files/board.txt";
	private static final String MOVE_FILE = "/Users/jessemorris/Desktop/Code/project_grand_autism/board_text_files/testing.txt";
	private static BufferedReader br;
	private static BufferedWriter bw;

	public static BoardStatusObject readBoard() {
		BoardStatusObject boardStatusObject = new BoardStatusObject();
		try {
			FileReader file = new FileReader(BOARD_STATUS);
			br = new BufferedReader(file);
			String line;
			while( (line = br.readLine()) != null) {
	    			String[] array = line.split(" ");
	    			if(array[0].contains("player")) {
	    				boardStatusObject.playersList.add(new Point(
	    					Integer.parseInt(array[1]), Integer.parseInt(array[2]), "Player"));
	    			}
	    			else if(array[0].contains("missle")) {
	    				boardStatusObject.pickupList.add(new Point(
	    					Integer.parseInt(array[1]), Integer.parseInt(array[2]), "Pick Up"));
	    			}
	    			else if(array[0].contains("wall")) {
	    				boardStatusObject.wallList.add(new Point(
	    					Integer.parseInt(array[1]), Integer.parseInt(array[2]), "walls"));
	    			}
			}
			br.close();
		}
		catch(IOException e) {
			System.err.println();
		}
		return boardStatusObject;

	}
	public static void writeMoveSet(String object, String direction) {
		try {
			FileWriter fw = new FileWriter(MOVE_FILE, true);
			bw = new BufferedWriter(fw);
			String string = (object + "-" + direction + " ");
			bw.write(string);
			bw.close();
		}
		catch(IOException e) {
			System.err.println();
		}


	}
}