class BoardObject():
	def __init__(self):
		self.playerList = []
		self.pickupList = []
		self.wallList = []

class Point():
	def __init__(self,x,y,type_string):
		self.x = x
		self.y = y
		self.type = type_string

	def toString(self):
		print(self.type + " " + self.x + " " + self.y)


board_status = "/Users/jessemorris/Desktop/Code/project_grand_autism/board_text_files/board.txt"
move_file = "/Users/jessemorris/Desktop/Code/project_grand_autism/board_text_files/testing.txt"

def readBoard():
	board_object = BoardObject()
	with open(board_status) as f:
		conent = f.readlines()

		for line in conent:
			array = line.split(" ")
			if "player" in array[0]:
				board_object.playerList.append(Point(array[1], array[2], "Player"))
			elif "missile" in array[0]:
				board_object.pickupList.append(Point(array[1], array[2], "Pick Up"))
			elif "wall" in array[0]:
				board_object.wallList.append(Point(array[1], array[2], "Wall"))

	return board_object

def writeMoveSet(thing, direction):
	with open(move_file, "a+") as f:
		f.write(thing + "-" + direction + " ")

