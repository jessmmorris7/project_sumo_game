import sys
import importlib

if __name__ == "__main__":
	name = sys.argv[1]
	game = importlib.import_module(name)
	game.run()

