#include <stdlib.h>
#include <stdio.h>
#include "IOforC.h"

#define BOARD_WIDTH 10
#define BOARD_HEIGHT 3

char* file_directory = "board.txt";
int** board;


int** read_Board() {
	char input[BOARD_WIDTH];
	FILE* board_file = fopen(file_directory, "r");
	board = (int**) malloc(BOARD_HEIGHT * sizeof(int*));
	for(int i = 0; i < BOARD_HEIGHT; i++) {
		board[i] = (int*) malloc(BOARD_WIDTH * sizeof(int));
		fscanf(board_file, "%s", input);
		for(int j = 0; j < BOARD_WIDTH; j++) {
			 board[i][j] = (input[j] - '0');
		}
	}
	fclose(board_file);
	return board;

}

void print_Board_memory() {
	for(int i = 0; i < BOARD_HEIGHT; i++) {
		for(int j = 0; j < BOARD_WIDTH; j++) {
			printf("%d", board[i][j]);
		}
		printf("\n");
	}

}

void main() {
	read_Board();
	print_Board_memory();
}

