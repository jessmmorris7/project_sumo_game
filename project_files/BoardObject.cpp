#include "BoardObject.hpp"
#include "boardFunctions.hpp"

Point BoardObject::getLocation() const {
    return m_location;
}

int BoardObject::getWidth() const {
    return m_width;
}

int BoardObject::getHeight() const {
    return m_height;
}

void BoardObject::setLocation(Point newLocation) {
    m_location = newLocation;
}

void BoardObject::move(Direction direction) {
    setLocation(next(getLocation(), direction));
}
