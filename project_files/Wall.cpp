#include "Wall.hpp"

void Wall::loseHealth(int amount) {
    m_health -= amount;
}

bool Wall::isDead() const {
    if (!m_indestructible && m_health <= 0) {
        return true;
    }
    return false;
}
