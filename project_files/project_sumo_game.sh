#!/bin/bash
g++ -std=c++11 -c Board.cpp boardFunctions.cpp BoardObject.cpp fileIO.cpp Player.cpp project_sumo_game.cpp Pickup.cpp Putdown.cpp run_algo.cpp Wall.cpp
g++ Board.o boardFunctions.o BoardObject.o fileIO.o Player.o project_sumo_game.o Pickup.o Putdown.o run_algo.o Wall.o -o project-sumo-game-app -framework sfml-graphics -framework sfml-window -framework sfml-system
