#include "fileIO.hpp"

std::string getLastLine(const char * pathed_file_name) {
    std::ifstream file;
    std::string line;
    file.open(pathed_file_name);
    if (file.is_open()) {
        while(getline(file, line)) {} // get the last line
        file.close();
    } else {
        std::cout << "Cannot open " << pathed_file_name << std::endl;
        return "-1";
    }
    return line;
}

void clearFile(const char * pathed_file_name) {
    std::ofstream file;
    file.open(pathed_file_name);
    if (file.is_open()) {
        file << "";
        file.close();
    } else {
        std::cout << "Cannot open " << pathed_file_name << std::endl;
    }
}

void addLine(const char * pathed_file_name, std::string string, bool newLine) {
    std::ofstream file;
    file.open(pathed_file_name);
    if (file.is_open()) {
        file << string;
        if (newLine) {
            file << '\n';
        }
        file.close();
    } else {
        std::cout << "Cannot open " << pathed_file_name << std::endl;
    }
}

void addNewline(const char * pathed_file_name) {
    std::ofstream file;
    file.open(pathed_file_name, std::ofstream::out | std::ofstream::app);
    if (file.is_open()) {
        file << '\n';
        file.close();
    } else {
        std::cout << "Cannot open " << pathed_file_name << std::endl;
    }
}
