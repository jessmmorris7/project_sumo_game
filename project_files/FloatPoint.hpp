#ifndef FLOAT_POINT
#define FLOAT_POINT

class FloatPoint {
public:
    float x;
    float y;
    FloatPoint(float x, float y)
    : x{x}, y{y}
    {};
};

#endif
