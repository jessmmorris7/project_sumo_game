#ifndef BOARD_OBJECT
#define BOARD_OBJECT

#include <iostream>
#include "Point.hpp"
#include "enumerations.h"

class BoardObject {
protected:
    Point m_location;
    int m_width;
    int m_height;

public:
    BoardObject(Point location, int width, int height)
    :m_location{location}, m_width{width}, m_height{height}
    {};
    BoardObject(int x, int y, int width, int height)
    :m_location{Point(x,y)}, m_width{width}, m_height{height}
    {};
    ~BoardObject(){}
    Point getLocation() const;
    int getWidth() const;
    int getHeight() const;
    void setLocation(Point newLocation);
    void move(Direction direction);
};

#endif
