#include "run_algo.hpp"

// C++ includes
#include <string>
#include <iostream>

// Forking includes
#include <unistd.h>
#include <fcntl.h>

// Do the thing
std::string runAlgo(std::string name){

    // Make pipe and fork
    int fwd[2];
    pipe(fwd);
    int pid = fork();

    // Child
    if(pid == 0){
        close(1);
        dup(fwd[1]);
        execlp("../run.sh", "run.sh", name.c_str(), NULL);
    }

    // Parent
    char buffer[10];
    read(fwd[0], buffer, 10);
    std::string ans(buffer);
    return ans;
}
