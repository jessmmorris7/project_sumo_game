#ifndef PLAYER
#define PLAYER

#include <iostream>
#include <vector>
#include "Point.hpp"
#include "constants.h"
#include "Pickup.hpp"
#include "BoardObject.hpp"
#include <cassert>

class Player : public BoardObject {
private:
    int m_health{c_startingPlayerHealth};
    std::vector<Pickup> m_backpack;
    int m_speed;
    std::string m_name;

public:
    Player(Point location)
    : BoardObject(location, c_playerSize, c_playerSize)
    {};
    Player(Point location, int health, int speed, std::string name)
    : BoardObject(location, c_playerSize, c_playerSize), m_health{health}, m_speed{speed}, m_name{name}
    {};
    ~Player(){}

    void loseHealth(int amount);
    bool isDead() const;
    void addToBackpack(Pickup &pickup);
    void removeFromBackpack(int index);
    int getSpeed() const;
    std::string getName() const;
};

#endif
