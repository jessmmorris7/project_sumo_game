#ifndef ENUMS
#define ENUMS

enum GameMode {
    VISUALISE_ONLY,
    FFA,
    NUM_GAME_MODES
};

enum MapSize {
    SMALL,
    NUM_MAP_SIZES
};

enum Direction {
    NORTH,
    NORTH_EAST,
    EAST,
    SOUTH_EAST,
    SOUTH,
    SOUTH_WEST,
    WEST,
    NORTH_WEST,
    NUM_DIRECTIONS
};

#endif
