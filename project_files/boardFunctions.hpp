#ifndef BOARD_FUNCTIONS
#define BOARD_FUNCTIONS

#include "Point.hpp"
#include "BoardObject.hpp"
#include "enumerations.h"

Point next(Point point, Direction direction);
bool overlaps(Point topLeft1, int width1, int height1, Point topLeft2, int width2, int height2);
bool overlaps(BoardObject &obj1, BoardObject &obj2);

#endif
