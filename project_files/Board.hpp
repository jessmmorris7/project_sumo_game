#ifndef BOARD
#define BOARD

#include <vector>
#include "enumerations.h"
#include "Pickup.hpp"
#include "Player.hpp"
#include "Point.hpp"
#include "Putdown.hpp"
#include "Wall.hpp"

class Board {
private:
    int m_rows;
    int m_cols;
    std::vector<Player> m_players;
    std::vector<Direction> m_nextMoves;
    std::vector<Pickup> m_pickups;
    std::vector<Putdown> m_putdowns;
    std::vector<Wall> m_walls;
    void setSize(MapSize mapSize);
    void updatePlayerLocation(int index, Point newLocation);
    void updatePickupLocation(int index, Point newLocation);
    void updatePutdownLocation(int index, Point newLocation);

public:
    Board(MapSize mapSize) {
        setSize(mapSize);
        generate();
    }
    void addNextMove(std::string direction);
    void clearMoves();
    void updateState();
    void writeBoardToFile() const;
    void generate();
};

#endif
