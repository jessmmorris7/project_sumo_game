#ifndef POINT
#define POINT

class Point {
public:
    int x;
    int y;
    Point(int x, int y)
    :x{x}, y{y}
    {};
};

#endif
