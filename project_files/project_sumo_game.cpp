#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include "ResourcePath.hpp"

#include <iostream>
#include <vector>
#include "stdlib.h"

#include "enumerations.h"
#include "run_algo.hpp"
#include "Board.hpp"
#include "fileIO.hpp"

int main(int argc, char ** argv) {

    // Parse inputs
    if (argc < 5) {
        std::cout << "ERROR: not enough input arguments, minimum is four," << std::endl;
        std::cout << " in form <gameMode> <mapSize> <player1Name> <player2Name>" << std::endl;
        return -1;
    }

    // Parse game type
    GameMode gameMode;
    std::string gameModeString = argv[1];
    if (gameModeString.compare("visual") == 0) {
        gameMode = VISUALISE_ONLY;
    } else if (gameModeString.compare("ffa") == 0) {
        gameMode = FFA;
    } else {
        std::cout << "ERROR: argument 1 is not a valid game mode" << std::endl;
        return -1;
    }
    // Parse map size
    MapSize mapSize;
    std::string mapSizeString = argv[2];
    if (mapSizeString.compare("small") == 0) {
        mapSize = SMALL;
    } else {
        std::cout << "ERROR: argument 2 is not a valid map size" << std::endl;
        return -1;
    }

    // Create player bash file names
    std::vector<std::string> teamNames;
    int numPlayers{argc-3};
    for (int i = 3; i < argc; ++i) {
        teamNames.push_back(argv[i]);
    }

    // Run game
    bool gameRunning{true};
    if (gameMode == VISUALISE_ONLY) {
        gameRunning = false;
    }

    if (gameRunning) {
        // Run compile of other programs
        system("./compile.sh");
        // Create the board
        Board board(mapSize);

        while (gameRunning) {
            // Write Board to file
            board.writeBoardToFile();
            // moveSet contains all moves in order of players separated by a space
            // 1st player
            std::string moveSet = runAlgo(teamNames[0]);
            board.addNextMove(moveSet);
            // other players
            for (int i = 1; i < numPlayers; ++i) {
                std::string playerMove = runAlgo(teamNames[i]);
                moveSet = moveSet + " " + playerMove;
                board.addNextMove(playerMove);
            }
            board.updateState();
            board.clearMoves();

            // Add line if it is valid
            addLine("text_files/moves.txt", moveSet, true);
        }
    }

}
