#ifndef PUTDOWN
#define PUTDOWN

#include "Point.hpp"
#include "constants.h"
#include "BoardObject.hpp"

class Putdown : public BoardObject {
protected:
    int m_speed{c_defaultSpeed}; // number of moves per turn
    Direction m_direction{c_defaultDirection};
    int m_damage{c_defaultDamage};

public:
    Putdown(Point location)
    : BoardObject(location, c_putdownSize, c_putdownSize)
    {};
    Putdown(Point location, int speed, Direction direction)
    : BoardObject(location, c_putdownSize, c_putdownSize), m_speed{speed}, m_direction{direction}
    {};
    Putdown(Point location, int speed, Direction direction, int damage)
    : BoardObject(location, c_putdownSize, c_putdownSize), m_speed{speed}, m_direction{direction}, m_damage{damage}
    {};
    int getSpeed() const;
    Direction getDirection() const;
    int getDamage() const;
    std::string getName() const;
};

#endif
