#ifndef WALL
#define WALL

#include "constants.h"
#include "Point.hpp"
#include "BoardObject.hpp"

class Wall : public BoardObject {
private:
    int m_health;
    bool m_indestructible;

public:
    Wall(Point location)
    : BoardObject(location, c_wallSize, c_wallSize), m_health{0}, m_indestructible{true}
    {};
    Wall(Point location, int health, bool indestructible)
    : BoardObject(location, c_wallSize, c_wallSize), m_health{health}, m_indestructible{indestructible}
    {
        if (!m_indestructible && m_health <= 0) {
            m_health = 1;
        }
    };

    void loseHealth(int amount);
    bool isDead() const;
};

#endif
