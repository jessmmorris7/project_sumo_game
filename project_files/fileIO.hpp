#ifndef fileIO_hpp
#define fileIO_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>

std::string getLastLine(const char * file_name);
void clearFile(const char * pathed_file_name);
void addLine(const char * pathed_file_name, std::string string, bool newLine);
void addNewline(const char * pathed_file_name);

#endif
