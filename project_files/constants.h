#ifndef CONSTANTS
#define CONSTANTS

#include "enumerations.h"

static const int c_playerSize{3};
static const int c_pickupSize{3};
static const int c_wallSize{1};
static const int c_putdownSize{1};
static const int c_startingPlayerHealth{1};
static const int c_defaultSpeed{1};
static const Direction c_defaultDirection{NORTH};
static const int c_defaultDamage{1};
static const int c_incrementsPerTurn{100};
static const char * c_boardFilePath = "../text_files/board.txt";

#endif
