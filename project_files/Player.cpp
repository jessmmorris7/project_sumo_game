#include "Player.hpp"

void Player::loseHealth(int amount) {
    m_health -= amount;
}

bool Player::isDead() const {
    if (m_health <= 0) {
        return true;
    }
    return false;
}

void Player::addToBackpack(Pickup &pickup) {
    m_backpack.push_back(pickup);
}

void Player::removeFromBackpack(int index) {
    assert(index < m_backpack.size());
    m_backpack.erase(m_backpack.begin() + index);
}

int Player::getSpeed() const {
    return m_speed;
}

std::string Player::getName() const {
    return m_name;
}
