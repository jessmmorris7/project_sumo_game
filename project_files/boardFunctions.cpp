#include <iostream>
#include "boardFunctions.hpp"

Point next(Point point, Direction direction) {
    switch (direction) {
        case NORTH:
            return Point(point.x, point.y - 1);
            break;
        case NORTH_EAST:
            return Point(point.x + 1, point.y - 1);
            break;
        case EAST:
            return Point(point.x + 1, point.y);
            break;
        case SOUTH_EAST:
            return Point(point.x + 1, point.y + 1);
            break;
        case SOUTH:
            return Point(point.x, point.y + 1);
            break;
        case SOUTH_WEST:
            return Point(point.x - 1, point.y + 1);
            break;
        case WEST:
            return Point(point.x - 1, point.y);
            break;
        case NORTH_WEST:
            return Point(point.x - 1, point.y - 1);
            break;
        case NUM_DIRECTIONS:
            // fall through
        default:
            std::cout << "ERROR: direction not valid in BoardObject.cpp" << std::endl;
            return Point(0,0);
    }
}

// note that topleft of screen is (0,0) and x increases moving horizontally and y increases moving vertically
bool overlaps(Point topLeft1, int width1, int height1, Point topLeft2, int width2, int height2) {
    for (int x = topLeft1.x; x < topLeft1.x + width1; ++x) {
        for (int y = topLeft1.y; y < topLeft1.y + height1; ++y) {
            if (x >= topLeft2.x && x < topLeft2.x - width2 && y >= topLeft2.y && y < topLeft2.y + height2) {
                return true;
            }
        }
    }
    return false;
}

bool overlaps(BoardObject &obj1, BoardObject &obj2) {
    return overlaps(obj1.getLocation(), obj1.getWidth(), obj1.getHeight(), obj2.getLocation(), obj2.getWidth(), obj2.getHeight());
}
