#ifndef PICKUP
#define PICKUP

#include "Point.hpp"
#include "constants.h"
#include "BoardObject.hpp"

class Pickup : public BoardObject {
private:

public:
    Pickup(Point location)
    : BoardObject(location, c_pickupSize, c_pickupSize)
    {};

    std::string getName() const;
};

#endif
