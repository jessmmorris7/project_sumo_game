#include <iostream>
#include <cmath>
#include "Board.hpp"
#include "constants.h"
#include "FloatPoint.hpp"
#include "boardFunctions.hpp"
#include "fileIO.hpp"
#include <cassert>

#define N_PICKUPS 8
#define N_WALLS 32

// Generate terrain
void Board::generate(){

    // Give RNGesus your seed
    srand(time(NULL));
    int x;
    int y;
    bool valid;

    // Init level to empty
    bool level[m_rows][m_cols];
    for(int i = 0; i < m_rows; i++){
        for(int j = 0; j < m_cols; j++)
            level[i][j] = false;
    }

    // Load players
    // 4 player small
    if(m_rows == 32 && m_cols == 32){
        m_players.push_back(Player(Point(7,7), 5, 5, "Player1"));
        m_players.push_back(Player(Point(7,23), 5, 5, "Player2"));
        m_players.push_back(Player(Point(23,7), 5, 5, "Player3"));
        m_players.push_back(Player(Point(23,23), 5, 5, "Player4"));
    }

    // Add players to level
    for(int i = 0; i < m_players.size(); i++){
        Point loc = m_players[i].getLocation();
        for(int j = 0; j < m_players[i].getWidth(); j++){
            for(int k = 0; k < m_players[i].getHeight(); k++)
                level[loc.y + k][loc.x + j] = true;
        }
    }


    // Place walls
    while(m_walls.size() < N_WALLS){

        // Generate pos and check if valid
        y = rand()%(m_rows - c_wallSize);
        x = rand()%(m_cols - c_wallSize);
        if(x < 0) x = 0;
        if(y < 0) y = 0;
        valid = true;
        for(int i = 0; i < c_wallSize; i++){
            for(int j = 0; j < c_wallSize; j++){
                if(level[y+i][x+j])
                    valid = false;
            }
        }

        // If valid: place
        if(!valid) continue;
        m_walls.push_back(Wall(Point(x,y)));
        for(int i = 0; i < c_wallSize; i++){
            for(int j = 0; j < c_wallSize; j++)
                level[y+i][x+j] = true;
        }

        // Cluster
        while(rand()%5 > 2){

            // Generate new pos
            if(rand()%2)
                x += -c_wallSize + 2*c_wallSize*(rand()%2);
            else
                y += -c_wallSize + 2*c_wallSize*(rand()%2);
            if(x < 0) x = 0;
            if(y < 0) y = 0;
            x = x%(m_rows - c_wallSize);
            y = y%(m_cols - c_wallSize);


            // Check valid
            valid = true;
            for(int i = 0; i < c_wallSize; i++){
                for(int j = 0; j < c_wallSize; j++){
                    if(level[y+i][x+j])
                        valid = false;
                }
            }

            // If valid: place
            if(!valid) continue;
            m_walls.push_back(Wall(Point(x,y)));
            for(int i = 0; i < c_wallSize; i++){
                for(int j = 0; j < c_wallSize; j++)
                    level[y+i][x+j] = true;
            }

        }
    }

    // Place pickups
    while(m_pickups.size() < N_PICKUPS){

        // Generate pos and check if valid
        y = rand()%(m_rows - c_wallSize);
        x = rand()%(m_cols - c_wallSize);
        if(x < 0) x = 0;
        if(y < 0) y = 0;
        valid = true;
        for(int i = 0; i < c_pickupSize; i++){
            for(int j = 0; j < c_pickupSize; j++){
                if(level[y+i][x+j])
                    valid = false;
            }
        }

        // If valid: place
        if(!valid) continue;
        m_pickups.push_back(Pickup(Point(x,y)));
        for(int i = 0; i < c_pickupSize; i++){
            for(int j = 0; j < c_pickupSize; j++)
                level[y+i][x+j] = true;
        }

    }
    
    std::cout << "Done!\n";

}

void Board::addNextMove(std::string direction) {
    if (direction == "NN") {
        m_nextMoves.push_back(NORTH);
    } else if (direction == "NE") {
        m_nextMoves.push_back(NORTH_EAST);
    } else if (direction == "EE") {
        m_nextMoves.push_back(EAST);
    } else if (direction == "SE") {
        m_nextMoves.push_back(SOUTH_EAST);
    } else if (direction == "SS") {
        m_nextMoves.push_back(SOUTH);
    } else if (direction == "SW") {
        m_nextMoves.push_back(SOUTH_WEST);
    } else if (direction == "WW") {
        m_nextMoves.push_back(WEST);
    } else if (direction == "NW") {
        m_nextMoves.push_back(NORTH_WEST);
    } else {
        std::cout << "ERROR: \"" << direction << "\" is not a valid direction." << std::endl;
        std::cout << "Valid directions are NN, NE, EE, SE, SS, SW, WW, NW." << std::endl;
        m_nextMoves.push_back(NORTH);
    }
}

void Board::clearMoves() {
    m_nextMoves.clear();
}

void Board::setSize(MapSize mapSize) {
    switch (mapSize) {
        case SMALL:
            m_rows = 32;
            m_cols = 32;
            break;
        case NUM_MAP_SIZES:
            // fall through
        default:
            std::cout << "ERROR: Map size not defined in board.cpp" << std::endl;
    }
}

void Board::updatePlayerLocation(int index, Point newLocation) {
    assert(m_players.size() < index);
    m_players[index].setLocation(newLocation);
}

void Board::updatePickupLocation(int index, Point newLocation) {
    assert(m_pickups.size() < index);
    m_pickups[index].setLocation(newLocation);
}

void Board::updatePutdownLocation(int index, Point newLocation) {
    assert(m_putdowns.size() < index);
    m_putdowns[index].setLocation(newLocation);
}

void Board::updateState() {
    std::vector<FloatPoint> playerCoords;
    std::vector<FloatPoint> putdownCoords;
    // Initialise all player and putdown coordinates
    for (int i = 0; i < m_players.size(); ++i) {
        playerCoords.push_back(FloatPoint(m_players[i].getLocation().x, m_players[i].getLocation().y));
    }
    for (int i = 0; i < m_putdowns.size(); ++i) {
        putdownCoords.push_back(FloatPoint(m_putdowns[i].getLocation().x, m_putdowns[i].getLocation().y));
    }

    for (int i = 0; i < c_incrementsPerTurn; ++i) {
        // move players a tiny amount
        for (int j = 0; j < m_players.size(); ++j) {
            Direction playerDirection{m_nextMoves[j]};
            if (playerDirection == NORTH) {
                playerCoords[j].y -= m_players[j].getSpeed()/c_incrementsPerTurn;
            } else if (playerDirection == SOUTH) {
                playerCoords[j].y += m_players[j].getSpeed()/c_incrementsPerTurn;
            } else if (playerDirection == EAST) {
                playerCoords[j].x += m_players[j].getSpeed()/c_incrementsPerTurn;
            } else if (playerDirection == WEST) {
                playerCoords[j].x -= m_players[j].getSpeed()/c_incrementsPerTurn;
            } else if (playerDirection == NORTH_EAST) {
                playerCoords[j].x += (m_players[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
                playerCoords[j].y -= (m_players[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
            } else if (playerDirection == NORTH_WEST) {
                playerCoords[j].x -= (m_players[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
                playerCoords[j].y -= (m_players[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
            } else if (playerDirection == SOUTH_EAST) {
                playerCoords[j].x += (m_players[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
                playerCoords[j].y += (m_players[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
            } else if (playerDirection == SOUTH_WEST) {
                playerCoords[j].x += (m_players[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
                playerCoords[j].y -= (m_players[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
            }

            // check next location doesn't collide with walls. If so, round back
            for (int w = 0; w < m_walls.size(); ++w) {
                if (overlaps(next(m_players[j].getLocation(), playerDirection), c_playerSize, c_playerSize, m_walls[w].getLocation(), c_wallSize, c_wallSize)) {
                    playerCoords[j].x = round(playerCoords[j].x); // round back
                    playerCoords[j].y = round(playerCoords[j].y); // round back
                }
            }

            // change grid position if rounds to new location
            if (round(playerCoords[j].x) != m_players[j].getLocation().x || round(playerCoords[j].y) != m_players[j].getLocation().y) {
                m_players[j].setLocation(Point(round(playerCoords[j].x), round(playerCoords[j].y)));
            }
        }
        // move putdowns a tiny amount
        for (int j = 0; j < m_putdowns.size(); ++j) {
            Direction putdownDirection{m_putdowns[j].getDirection()};
            if (putdownDirection == NORTH) {
                putdownCoords[j].y -= m_putdowns[j].getSpeed()/c_incrementsPerTurn;
            } else if (putdownDirection == SOUTH) {
                putdownCoords[j].y += m_putdowns[j].getSpeed()/c_incrementsPerTurn;
            } else if (putdownDirection == EAST) {
                putdownCoords[j].x += m_putdowns[j].getSpeed()/c_incrementsPerTurn;
            } else if (putdownDirection == WEST) {
                putdownCoords[j].x -= m_putdowns[j].getSpeed()/c_incrementsPerTurn;
            } else if (putdownDirection == NORTH_EAST) {
                putdownCoords[j].x += (m_putdowns[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
                putdownCoords[j].y -= (m_putdowns[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
            } else if (putdownDirection == NORTH_WEST) {
                putdownCoords[j].x -= (m_putdowns[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
                putdownCoords[j].y -= (m_putdowns[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
            } else if (putdownDirection == SOUTH_EAST) {
                putdownCoords[j].x += (m_putdowns[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
                putdownCoords[j].y += (m_putdowns[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
            } else if (putdownDirection == SOUTH_WEST) {
                putdownCoords[j].x += (m_putdowns[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
                putdownCoords[j].y -= (m_putdowns[j].getSpeed()/sqrt(2))/c_incrementsPerTurn;
            }
            // change grid position if rounds up
            if (round(putdownCoords[j].x) != m_putdowns[j].getLocation().x || round(putdownCoords[j].y) != m_putdowns[j].getLocation().y) {
                m_putdowns[j].setLocation(Point(round(putdownCoords[j].x), round(putdownCoords[j].y)));
            }
        }

        // check collisions
        int k = 0;
        // Loop through putdowns
        while (k < m_putdowns.size()) {
            bool collision{false}; // needed so that walls code only runs if there is no player collision
            // Loop through players
            int j = 0;
            while (j < m_players.size()) {
                // check overlap with players
                if (overlaps(m_putdowns[k], m_players[j])) {
                    collision = true;
                    m_players[j].loseHealth(m_putdowns[k].getDamage());
                    if (m_players[j].isDead()) {
                        m_players.erase(m_players.begin() + j--);
                    }
                    // remove the putdown
                    m_putdowns.erase(m_putdowns.begin() + k);
                    break; // from player loop
                }
                ++j;
            }
            if (collision) {
                continue; // k should not be incremented/decremented
            }

            // Loop through walls
            int w = 0;
            while (w < m_walls.size()) {
                // check overlap with walls
                if (overlaps(m_putdowns[k], m_walls[w])) {
                    collision = true;
                    m_walls[w].loseHealth(m_putdowns[k].getDamage());
                    if (m_walls[w].isDead()) {
                        m_walls.erase(m_walls.begin() + w--);
                    }
                    // remove the putdown
                    m_putdowns.erase(m_putdowns.begin() + k);
                    break; // from wall loop
                }
                ++w;
            }
            if (collision) {
                continue; // k should not be incremented/decremented
            }

            ++k;
        }
    }
}

/*void Board::updateState() {
    // move all the putdowns
    for (int i = 0; i < m_putdowns.size(); ++i) {
        for (int j = 0; j < m_putdowns[i].getSpeed(); ++j) {
            m_putdowns[i].move(m_putdowns[i].getDirection());
            bool isDeleted{false};
            // check collision with other objects, first walls
            for (int k = 0; k < m_walls.size(); ++k) {
                if (overlaps(m_putdowns[i], m_walls[k]) {
                    // overlap triggered, delete putdown
                    m_putdowns.erase(i);
                    isDeleted = true;
                    break;
                }
            }
            if (isDeleted) {
                break;
            }
            for (int k = 0; k < m_players.size(); ++k) {
                if (overlaps(m_putdowns[i], m_players[k]) {
                    // overlap triggered
                    m_players[k].loseHealth(m_putdowns[i].getDamage());
                    if (m_players[k].isDead()) {
                        m_players.erase(k);
                        m_nextMoves.erase(k);
                    }
                    m_putdowns.erase(i);
                    isDeleted = true;
                    break;
                }
            }
            if (isDeleted) {
                break;
            }
        }
    }

    // Move all the players unless they hit walls
    int count{0};
    bool movePlayers{true};
    while (movePlayers) {
        movePlayers = false;
        for (int i = 0; i < m_players.size(); ++i) {
            Direction playerMove{m_nextMoves[i]};
            if (count < m_players[i].getSpeed()) {
                // check each wall
                bool collision{false};
                for (int w = 0; w < m_walls.size(); ++w) {
                    // check collision
                    if (overlaps(next(m_players.getLocation(), playerMove), c_playerSize, c_playerSize, m_walls[w], c_wallSize, c_wallSize)) {
                        collision = true;
                    }
                }
                if (!collision) {
                    movePlayers = true;
                    updatePlayerLocation(i, next(m_players.getLocation(), playerMove));
                } else {
                    break;
                }
            }
        }
        ++count;
    }
}*/

void Board::writeBoardToFile() const {
    for (int i = 0; i < m_players.size(); ++i) {
        addLine(c_boardFilePath, m_players[i].getName() + " " + std::to_string(m_players[i].getLocation().x) + " " + std::to_string(m_players[i].getLocation().y), false);
        if (i == m_players.size()-1) {
            addNewline(c_boardFilePath);
        }
    }
    for (int i = 0; i < m_pickups.size(); ++i) {
        addLine(c_boardFilePath, m_pickups[i].getName() + " " + std::to_string(m_pickups[i].getLocation().x) + " " + std::to_string(m_pickups[i].getLocation().y), false);
        if (i == m_pickups.size()-1) {
            addNewline(c_boardFilePath);
        }
    }
    for (int i = 0; i < m_putdowns.size(); ++i) {
        addLine(c_boardFilePath, m_putdowns[i].getName() + " " + std::to_string(m_putdowns[i].getLocation().x) + " " + std::to_string(m_putdowns[i].getLocation().y), false);
        if (i == m_putdowns.size()-1) {
            addNewline(c_boardFilePath);
        }
    }
    for (int i = 0; i < m_walls.size(); ++i) {
        addLine(c_boardFilePath, "wall " + std::to_string(m_walls[i].getLocation().x) + " " + std::to_string(m_walls[i].getLocation().y), false);
        if (i == m_walls.size()-1) {
            addNewline(c_boardFilePath);
        }
    }
    addLine(c_boardFilePath, "-", true);
}
