#!/bin/bash

name=$1
script_name=

#cd "Player_Algos"
script_name="$(ls PlayerAlgorithms/$name.*)"
controller_path="PlayerAlgorithmControllers/"
#for compiling Java
#
# javac -d . ../PlayerAlgorithms/BoardGameAlgoTest.java 



if [[ $script_name = *".cpp"* ]]; then
	output=$(echo $script_name | sed 's/.\{4\}$//')
		$(g++ -std=c++11 $script_name -o $output)
elif [[ $script_name = *".c" ]]; then
	output=$(echo $script_name | sed 's/.\{2\}$//')
	$(gcc -std=c99 -o $output $script_name)
elif [[ $script_name = *".m"* ]]; then
	echo ".m"
elif [[ $script_name = *".py" ]]; then
	cp "PlayerAlgorithms/$name.py" "PlayerAlgorithmControllers/PythonSrc"
elif [[ $script_name = *".java" ]]; then
	cp "PlayerAlgorithms/$name.java" "PlayerAlgorithmControllers/JavaSrc"
	cd "PlayerAlgorithmControllers/JavaSrc"
	$(javac -d classes Point.java)
	$(javac -d classes BoardStatusObject.java)
	$(javac -d classes JavaClassLoader.java)
	$(javac -d classes BoardStatusController.java)
	$(javac -d classes BoardControllerJavaInterface.java)

	$(javac -d classes $name.java)

else 
	echo "You cannot run this file"
fi
