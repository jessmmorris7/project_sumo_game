#include<SFML/Window.hpp>
#include<SFML/Graphics.hpp>
#include<SFML/System.hpp>

#include<iostream>
#include<sstream>
#include<fstream>
#include<string>

#define FPS 3
#define SCALE ((float) BLOCK / 64)
#define BLOCK 16
#define RES_X RES_Y
#define RES_Y 960
#define LARGE_RES 5
#define SMALL_RES 3

// Helper functions
int getRotate(std::string dir);

int main(int argc, char** argv){
    
    // Check file: open or return
    if(argc != 2){
        std::cout << "Please provide the objects file\n";
        return 0;
    }

    // Open window
    sf::RenderWindow window(sf::VideoMode(RES_X, RES_Y), "Jesse is a cuck");
    window.setFramerateLimit(FPS);

    // Load textures
    sf::Texture player1Texture;
    player1Texture.loadFromFile("textures/player1.png");
    sf::Texture player2Texture;
    player2Texture.loadFromFile("textures/player2.png");
    sf::Texture player3Texture;
    player3Texture.loadFromFile("textures/player3.png");
    sf::Texture player4Texture;
    player4Texture.loadFromFile("textures/player4.png");
    sf::Texture wallTexture;
    wallTexture.loadFromFile("textures/brick.png");
    sf::Texture missileTexture;
    missileTexture.loadFromFile("textures/missile.png");
    sf::Texture missilePickupTexture;
    missilePickupTexture.loadFromFile("textures/missilePickup.png");

    // Create background
    sf::Texture backgroundTexture;
    backgroundTexture.loadFromFile("textures/background.png");
    sf::Sprite backgroundSprite(backgroundTexture);
    sf::Texture gridTexture;
    gridTexture.loadFromFile("textures/grid.png");
    gridTexture.setRepeated(true);
    sf::Sprite gridSprite;
    gridSprite.setTexture(gridTexture);
    gridSprite.setTextureRect({0,0,RES_X, RES_Y});

    // Get player names
    std::string player1;
    std::string player2;
    std::string player3;
    std::string player4;
    std::string line;
    std::istringstream iss;
    std::ifstream file(argv[1]);
    std::getline(file, line);
    iss.str(line);
    iss >> player1;
    std::getline(file, line);
    iss.str(line);
    iss >> player2;
    std::getline(file, line);
    iss.str(line);
    iss >> player3;
    std::getline(file, line);
    iss.str(line);
    iss >> player4;

    // Reset file
    file.seekg(0, std::ios::beg);

    // Process loop
    while(window.isOpen()){

        // Process events
        sf::Event event;
        while(window.pollEvent(event)){
            if(event.type == sf::Event::Closed)
                window.close();
        }

        // Pause at completion
        if(file.eof())
            continue;

        // Clear and draw background
        window.clear();
        window.draw(backgroundSprite);
        window.draw(gridSprite);

        // Process each line
        std::string line;
        while(std::getline(file, line)){

            // Break at end of that turn
            if(!line.compare("-"))
                break;

            // Parse info
            std::istringstream iss(line);
            std::string name;
            int x;
            int y;
            iss >> name >> x >> y;

            // Create sprite
            sf::Sprite sprite;
            sprite.setPosition(x*BLOCK,y*BLOCK);

            // Set texture and scale accoridingly
            if(!name.compare(player1)){
                sprite.setTexture(player1Texture); 
                sprite.setScale(sf::Vector2f(SCALE*LARGE_RES, SCALE*LARGE_RES));
            }
            else if(!name.compare(player2)){
                sprite.setTexture(player2Texture); 
                sprite.setScale(sf::Vector2f(SCALE*LARGE_RES, SCALE*LARGE_RES));
            }
            else if(!name.compare(player3)){
                sprite.setTexture(player3Texture); 
                sprite.setScale(sf::Vector2f(SCALE*LARGE_RES, SCALE*LARGE_RES));
            }
            else if(!name.compare(player4)){
                sprite.setTexture(player4Texture); 
                sprite.setScale(sf::Vector2f(SCALE*LARGE_RES, SCALE*LARGE_RES));
            }
            else if(!name.compare("wall")){
                sprite.setTexture(wallTexture); 
                sprite.setScale(sf::Vector2f(SCALE*SMALL_RES, SCALE*SMALL_RES));
            }
            else if(!name.compare("missile")){
                sprite.setTexture(missileTexture); 
                sprite.setScale(sf::Vector2f(SCALE*SMALL_RES, SCALE*SMALL_RES));

                // Set direction
                std::string dir;
                iss >> dir;
                sprite.setRotation(getRotate(dir));
                
            }
            else if(!name.compare("missile_pickup")){
                sprite.setTexture(missilePickupTexture); 
                sprite.setScale(sf::Vector2f(SCALE*LARGE_RES, SCALE*LARGE_RES));
            }         

            window.draw(sprite);

        }

        // Display everything
        window.display();

    }

    file.close();

}

// Get degrees from compass dir
int getRotate(std::string dir){
    if(!dir.compare("NN"))
        return 0;
    else if(!dir.compare("NE"))
        return 45;
    else if(!dir.compare("EE"))
        return 90;
    else if(!dir.compare("SE"))
        return 135;
    else if(!dir.compare("SS"))
        return 180;
    else if(!dir.compare("SW"))
        return 225;
    else if(!dir.compare("WW"))
        return 270;
    else if(!dir.compare("NW"))
        return 315;
}
